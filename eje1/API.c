#include "API.h"

void buyWrapper(char* movie, int amount, int hour){
	int fd, i;
	ansBuy * ans;
	struct flock lock;
	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;

	fd = open("AccessDataBase.txt",O_CREAT|O_RDWR, 0666);
	if ( fd == -1 )
		perror("open");	
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("MI ERROR lock: ");
	ans = buy(movie,amount,hour);

	if(ans != NULL){
	
		for(i=0; i<amount;i++){
			printf("idTickets: %d\n",ans->ticketID[i]);
		}
	}
	else{
		printf("Invalid query\n");
	}

	lock.l_type=F_UNLCK;
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("MI ERROR lock: ");
	close(fd);
	free(ans);

}

void showMoviesWrapper(){
	int fd, i;
	ansSM * ans;
	struct flock lock;
	lock.l_type = F_RDLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	fd = open("AccessDataBase.txt",O_CREAT|O_RDONLY, 0666);
	if ( fd == -1 )
		perror("open");	
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("MI ERROR lock: ");
	ans = showMovies();

	for(i=0; i<ans->amount;i++){
		printf("Movie: %s Hour: %d\n",ans->listFunctions[i].movie, ans->listFunctions[i].hour);
	}

	lock.l_type=F_UNLCK;
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("ERROR lock: ");
	close(fd);
	free(ans);
}

void availableSitsWrapper(char * movie, int hour){
	struct flock lock;
	int fd;
	ansAvailSits * ans;
	lock.l_type = F_RDLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	fd = open("AccessDataBase.txt",O_CREAT|O_RDONLY, 0666);
	if ( fd == -1 )
		perror("open");	
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("ERROR lock: ");	

	ans = availableSits(movie,hour);
	if (ans != NULL){

		printf("Available Sits for %s are %d\n",movie,ans->amount);
	
	}
	else{

		printf("Invalid query\n");
	}
	
	lock.l_type=F_UNLCK;
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("ERROR lock: ");
	close(fd);
	free(ans);

}

void returnTicketsWrapper(int idTickets[], int amount){
	struct flock lock;
	int fd;
	ansRetTickets * ans;
	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	fd = open("AccessDataBase.txt",O_CREAT|O_RDWR, 0666);
	if ( fd == -1 )
		perror("open");	
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("ERROR lock: ");	

	ans = returnTickets(idTickets, amount);

	if(ans->success==TRUE)
		printf("Operation successful\n");
	else
		printf("Operation NOT successful\n");

	lock.l_type=F_UNLCK;
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("ERROR lock: ");
	close(fd);
	free(ans);

}
void viewDescriptionWrapper(char* movie){
	struct flock lock;
	int fd;
	ansViewDescr * ans;
	lock.l_type = F_RDLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	fd = open("AccessDataBase.txt",O_CREAT|O_RDWR, 0666);
	if ( fd == -1 )
		perror("open");	
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("ERROR lock: ");

	ans = viewDescription(movie);

	if(ans != NULL){

		printf("Description: %s\n",ans->descr);
	
	}
	else{
		printf("Invalid query\n");
	}
	
	lock.l_type=F_UNLCK;
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("ERROR lock: ");
	close(fd);
	free(ans);
}
