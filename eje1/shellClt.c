#include "API.h"
#include <signal.h>
#include "../sqlite/sqlite3.h"

static char* commands[6] = {"buy","viewDescription","availableSits","returnTickets","showMovies", "help"};
extern char serverIP[15];

int main(int argc, char ** argv){

	while(1){
		printf("\n******************************************************\n");
		printf("enter command: ");
		char* currentCommand;
		currentCommand= malloc(LENGTH_MOVIE);
		scanf("%s",currentCommand);
		int i,j;
		int found=0;
		for (i = 0; i < COUNT_COMMANDS; i++){
			j = strcmp(currentCommand,commands[i]);
			if(j==0){
				found=1;
				switch(i){
					case(0):
						execBuy();
						break;
					
					case(1):
						execViewDesc();
						break;
					
					case(2):
						execAvSits();
						break;
					
					case(3):
						execRetTickets();
						break;
					
					case(4):
						execShowMoies();
						break;
					
					case(5):
						execHelp();
						break;
					
					default:
						break;

				}		
					
			}

		}

		if(found==0){
			printf("incorrect command\n");
		} 
	}
	return 0;
}

void execBuy(void){
	char movie[LENGTH_MOVIE];
	int hour, amount, aux;
	printf("enter movie name: ");	
	scanf("%s",movie);
	printf("enter hour: ");
	do
	{
		aux = scanf("%d",&hour);
		if((aux <0) || (hour<0) || (hour>23)){
			printf("incorrect parameters, enter valid hour: ");
			aux = 0;
		}			
	}while(aux == 0);
	printf("enter amount: ");
	do{
		aux= scanf("%d",&amount);
		if(aux<0 || amount<0){
			printf("incorrect parameters, enter valid amount: ");
			aux = 0;
		}	
	}while(aux == 0);

	buyWrapper(movie,amount,hour);
	return;
}

void execViewDesc(void){
	char movie[LENGTH_MOVIE];
	printf("enter name of movie: ");
	scanf("%s",movie);
	viewDescriptionWrapper(movie);
	return;
}

void execAvSits(void){
	int hour, aux;
	char movie[LENGTH_MOVIE];
	printf("enter name of movie: ");
	printf("enter name of movie: ");
	scanf("%s",movie);
	printf("enter hour: ");
	do
	{
		aux = scanf("%d",&hour);
		if((aux <0) || (hour<0) || (hour>23)){
			printf("incorrect parameters, enter valid hour: ");
			aux = 0;
		}			
	}while(aux == 0);
	availableSitsWrapper(movie,hour);
	return;
}

void execShowMoies(void){

	showMoviesWrapper();
}

void execRetTickets(void){
	int amount, i;
	int tickets[RANDOM_NUM];
	printf("how many ticket do you want to return?\n");
	while((scanf("%d",&amount))<0){
		printf("incorrect, enter valid ampount: ");
	}

	for (i = 0; i < amount; i++){
		printf("enter number %d of your tickets: \n",i+1);
		while(scanf("%d",&(tickets[i]))<0){
			printf("incorrect number, enter valid number: ");
		}
					
	}
	returnTicketsWrapper(tickets,amount);
	return;
}

void execHelp(void){
	printf("command:\nbuy: buy tickets for only one movie.\n"
			"viewDescription: see the description of a movie.\n"
			"avaibleSits: see the available seats of one function.\n"
			"showMovies: see all movies and schedules.\n"
			"returnTickets: return any ticket you have bought.\n");
	return;
}



