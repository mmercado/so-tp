#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

/*
 * Includes
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"


#define MAX_SIZE 400
#define TRUE 1
#define FALSE 0

/*
 * Structs
*/

typedef struct{	
	int opCode;
} datagram;

/* Holds necesary data to start a connection between client and server */
typedef struct{
	int id;
} connection;


/*
 * Functions
*/


/*
 * Sends the operation serialized in a string.
*/
void sendMsg(char * args, int msgSize, char ans[MAX_SIZE]);

void acceptCon(connection * con);

char * receive(connection * con);

void respond(connection * con, char ans[MAX_SIZE], int ansSize);

void endServer();


#endif /* COMMUNICATION_H_ */