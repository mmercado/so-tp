#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "communication.h"
#include "../sqlite/sqlite3.h"

#define QUERY_SIZE 1000


char * queryAns;
function auxSM[FUNCTIONS_AMOUNT];
int countMovie;
int empty;

sqlite3 * db;
char * zErrMsg, * sql; 
char * data = "Callback called";
int rc;



void openDataBase(sqlite3 ** db);

static int callback(void *NotUsed, int argc, char **argv, char **azColName);

static int showMoviesHandler(void *NotUsed, int argc, char **argv, char **azColName);

void openDataBase(sqlite3 ** db){
	int rc;
	rc = sqlite3_open("cinema.db", db);

    if( rc ){

    	fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(*db));
    	exit(0);

	}

	return;	
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	strcpy(queryAns, argv[0]);
	empty = FALSE;
	return 0;
}

static int showMoviesHandler(void *NotUsed, int argc, char **argv, char **azColName){
        strcpy(auxSM[countMovie].movie,argv[0]);
        auxSM[countMovie++].hour = atoi(argv[1]);
        return 0;
}

void * buy(char * movie, int amount, int hour){

	int ticketID, availsits, i;
	empty = TRUE;

	zErrMsg = 0;
	sql = calloc(QUERY_SIZE, sizeof(char));
	queryAns = calloc(QUERY_SIZE, sizeof(char));
	
	ansBuy * ans = calloc(sizeof(ansBuy), sizeof(char));

	openDataBase(&db);

	sprintf(sql, "SELECT AVAILSITS from FUNCTIONS where NAME = '%s' AND "\
		"TIME = %d", movie, hour);

	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		exit(1);
	}

	sscanf(queryAns, "%d", &availsits);

	if( !empty && availsits >= amount){

		strcpy(sql, "SELECT TICKET from TICKETS");
		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
			exit(1);
		}

		sscanf(queryAns, "%d", &ticketID);
		
		sprintf(sql, "UPDATE TICKETS set TICKET = %d", (ticketID + amount) );

		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
			exit(1);
		}

		sprintf(sql, "UPDATE FUNCTIONS set AVAILSITS = %d " \
			"where NAME = '%s' AND TIME = %d", (availsits - amount), movie, hour);

		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
			exit(1);
		}


		for(i=0; i<amount; i++){
			sprintf(sql, "INSERT INTO SOLD(TICKETID, NAME, TIME) " \
				"VALUES (%d, '%s', %d)", ticketID + i, movie, hour);

			rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
			if( rc != SQLITE_OK ){
				fprintf(stderr, "SQL error: %s\n", zErrMsg);
				sqlite3_free(zErrMsg);
				exit(1);
			}

			ans->ticketID[i] = ticketID + i;
		}

	}
	
	sqlite3_close(db);
	free(queryAns);
	free(sql);

	if(empty){
		return NULL;
	}

	return ans;

}

void * availableSits(char * movie, int hour){

	empty = TRUE;

	queryAns = calloc(QUERY_SIZE, sizeof(char));
	zErrMsg = 0;
	sql = calloc(QUERY_SIZE, sizeof(char));
	
	ansAvailSits * ans = calloc(sizeof(ansAvailSits), sizeof(char));
	
	openDataBase(&db);

	sprintf(sql, "SELECT AVAILSITS from FUNCTIONS where NAME = '%s' AND "\
		"TIME = %d", movie, hour);

	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		exit(1);
	}

	if(!empty){ 
		sscanf(queryAns, "%d", &(ans->amount));
		
	}	

	sqlite3_close(db);
	free(queryAns);
	free(sql);

	if(empty){ 
		return NULL;
	}	

	return ans;

}


void * returnTickets(int idTickets[], int amount){

	int availsits, soldTickets = 0;
	empty = TRUE;

	queryAns = calloc(QUERY_SIZE, sizeof(char));
	zErrMsg = 0;
	sql = calloc(QUERY_SIZE, sizeof(char));

	ansRetTickets * ans = calloc(sizeof(ansRetTickets), sizeof(char));
	
	openDataBase(&db);


	// 1. Checks if all the tickets are in the SOLD table.
	int i;
	for( i=0; i < amount; i++ ){
		sprintf(sql, "SELECT TICKETID from SOLD where TICKETID = '%d'; ", idTickets[i]);

		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}
		
		if(empty){

			ans->success = FALSE;
			return ans;
		
		}

		sscanf(queryAns, "%d", &soldTickets);
	}


	for( i=0; i < amount; i++ ){
		
		sprintf(sql, "SELECT AVAILSITS from FUNCTIONS where " \
					 "NAME in (SELECT NAME from SOLD where TICKETID = %d) AND " \
					 "TIME in (SELECT TIME from SOLD where TICKETID = %d);"
					 , idTickets[i], idTickets[i]);

		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		}

		sscanf(queryAns, "%d", &availsits);


		sprintf(sql, "UPDATE FUNCTIONS set AVAILSITS = %d where " \
					 "NAME in (SELECT NAME from SOLD where TICKETID = %d) AND " \
					 "TIME in (SELECT TIME from SOLD where TICKETID = %d); " \
					 "DELETE from SOLD where TICKETID = %d;"
					 , availsits+1, idTickets[i], idTickets[i], idTickets[i]);

		rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if( rc != SQLITE_OK ){
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
			exit(1);
		}
	}

	sqlite3_close(db);
	free(queryAns); 
	free(sql);
	ans->success = TRUE;

	return ans;
}

void * viewDescription(char * movie){

	empty = TRUE;

	queryAns = calloc(QUERY_SIZE, sizeof(char));
	zErrMsg = 0;
	sql = calloc(QUERY_SIZE, sizeof(char));

	ansViewDescr * ans = calloc(sizeof(ansViewDescr), sizeof(char));

	openDataBase(&db);

	sprintf(sql, "SELECT DESCR from MOVIE where NAME = '%s'", movie);

	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		exit(1);
	}

	if(!empty){ 
		strcpy(ans->descr, queryAns);
	
	}	

	sqlite3_close(db);
	free(queryAns); 

	if(empty){ 
		return NULL;
	}	
		
	return ans;
}

void * showMovies(void){

	queryAns = calloc(QUERY_SIZE, sizeof(char));
	zErrMsg = 0;
	sql = calloc(QUERY_SIZE, sizeof(char));

	countMovie = 0;
	ansSM * ans = calloc(sizeof(ansSM), sizeof(char));
   	
    openDataBase(&db);

    sprintf(sql, "SELECT NAME,TIME from FUNCTIONS");

    rc = sqlite3_exec(db, sql, showMoviesHandler, (void*)data, &zErrMsg);
    if( rc != SQLITE_OK ){
            fprintf(stderr, "SQL error: %s\n", zErrMsg);
            sqlite3_free(zErrMsg);
            exit(1);
    }

    ans->amount = countMovie;
    memcpy((ans->listFunctions),auxSM,sizeof(ansSM));
    
    sqlite3_close(db);
    free(queryAns);
    free(sql);
    return ans;
}