#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "communication.h"


#define BUF_SIZE 100
#define COUNT_COMMANDS 6 
#define RANDOM_NUM 10
#define LENGTH_MOVIE 25

void execBuy(void);
void execViewDesc(void);
void execAvSits(void);
void execShowMoies(void);
void execRetTickets(void);
void execHelp(void);


void buyWrapper(char * movie, int amount, int hour);

void showMoviesWrapper(void);

void availableSitsWrapper(char * movie, int hour);

void returnTicketsWrapper(int idTickets[], int amount);

void viewDescriptionWrapper(char * movie);