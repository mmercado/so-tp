//common.h
#ifndef COMMON_H_
#define COMMON_H_

typedef enum {BUY=5, AVAILSITS, RETURN, VIEWDESCR, SHOWMOVIES} opCodes;

#define LENGTH_MOVIE 25
#define FUNCTIONS_AMOUNT 10

typedef struct
{	
	int hour;
	char movie[LENGTH_MOVIE];
	
} function;

typedef struct{
	int amount;
	int hour;
	char movie[50];
} argsBuy;

typedef struct{	
	
	int ticketID[15];

} ansBuy;

typedef struct{

	int hour;
	char movie[50];

} argsAvailSits;

typedef struct{

	int amount;

} ansAvailSits;

typedef struct{

	char movie[50];

} argsViewDescr;

typedef struct{
	
	char descr[50];

} ansViewDescr;


typedef struct{

    int amount;
    function listFunctions[FUNCTIONS_AMOUNT];
        
} ansSM;

typedef struct{

	int amount;
	int tickets[50];

} argsRetTickets;

typedef struct
{
	int success;

} ansRetTickets;

void * buy(char * movie, int amount, int hour);

void * showMovies(void);

void * availableSits(char * movie, int hour);

void * returnTickets(int idTickets[], int amount);

void * viewDescription(char * movie);


#endif /* COMMON_H_ */