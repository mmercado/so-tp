//queues.h
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "../communication.h"
#include <unistd.h>

typedef struct{
	long mtype;
	char mtext[1000];
} queueMsg;