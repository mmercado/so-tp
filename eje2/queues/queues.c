
#include "queues.h"

#define DEFAULT_CHANNEL 2
static key_t keyin = 0xBEEF0;
static key_t keyout = 0xBEEF1;
static int serverStarted = FALSE;
static int qin;
static int qout;


typedef enum { false, true } bool;



void
fatal(char *s)
{
	perror(s);
	exit(1);
}

//this method give me the only two queues to send and receive data
void startConnection(){
	if ( (qin = msgget(keyin, 0666|IPC_CREAT)) == -1 )
		fatal("Error msgget qin");

	if ( (qout = msgget(keyout, 0666|IPC_CREAT)) == -1 )
		fatal("Error msgget qout");
}


void sendMsg(char* msg, int size, char ans[MAX_SIZE]){
	
	startConnection();


	queueMsg * dato, * pidMsg;
	pidMsg = malloc(sizeof(queueMsg));
	dato = malloc(sizeof(queueMsg));
	dato->mtype = getpid();
	pidMsg->mtype = DEFAULT_CHANNEL;
	
	//send fist msg with type DEFAUL_CHANNEL with the pid and after that send a msg with type pid
	//this is for have compatibility with the others IPC
	memcpy(dato->mtext, msg, size+strlen(msg)+1);

	msgsnd(qout,pidMsg,sizeof(pidMsg->mtext), 0);
	
	msgsnd(qout, dato,sizeof(dato->mtext), 0);


	msgrcv(qin, dato, MAX_SIZE, dato->mtype, 0);
		

	memcpy(ans,&(dato->mtext),atoi(dato->mtext)+strlen(dato->mtext)+1);

	free(dato);	

	free(pidMsg);
}


void acceptCon(connection * con){
	//this method receive msg with type DEFAULT_CHANNEL with pids of clients
	
	queueMsg * queueMsgrcv;
	
	queueMsgrcv = calloc(sizeof(queueMsg),sizeof(char));
	
	if(!serverStarted){
		startConnection();
		serverStarted = TRUE;
	}
	
	msgrcv(qout, queueMsgrcv,sizeof(queueMsgrcv->mtext), DEFAULT_CHANNEL, 0);
	
	sscanf(queueMsgrcv->mtext, "%d", &(con->id));

	free(queueMsgrcv);
}


char * receive(connection* con){
	//this method receive msg with type pid

	queueMsg * queueMsgrcv;
	
	queueMsgrcv = calloc(sizeof(queueMsg),sizeof(char));

	msgrcv(qout, queueMsgrcv,sizeof(queueMsgrcv->mtext), con->id, 0);
	
	con->id=queueMsgrcv->mtype;
	
	return queueMsgrcv->mtext;
}


void respond(connection * con, char ans[MAX_SIZE], int ansSize){
	//this method respond msg with type pid
	
	queueMsg * respond;
	
	respond= calloc(sizeof(queueMsg),sizeof(char));
	
	respond->mtype=con->id;
	
	memcpy(&(respond->mtext),ans, ansSize +strlen(ans)+1);
	
	msgsnd(qin, respond, MAX_SIZE, 0);

	free(respond);
}


void endServer(){
	//remove queues when the server receive a signal
	msgctl(qin, IPC_RMID, NULL);
	
	msgctl(qout, IPC_RMID, NULL);
}
