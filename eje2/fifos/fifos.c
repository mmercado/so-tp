//fifos.c

#include "fifos.h"
static int pidFather;



void acceptCon(connection* con){
	int fdConnFifo;
	int pidCli;
	char buf[MAX_INT_LENGTH];


	//The fifo is created if it is the first time the method accept is called
	if( !serverStarted ){

		pidFather = getpid();

		createFifo(connectionFifo);

		serverStarted = TRUE;

	}

	fdConnFifo = open(connectionFifo, O_RDONLY);

	getStr(fdConnFifo, buf, '\0');
	
	pidCli = atoi(buf);
	
	con->id = pidCli;

	close(fdConnFifo);

	return;
}


void sendMsg(char* msg, int size, char ans[MAX_SIZE]){
	int connFifo, pidClient, sizePid, fdWriteFifo, fdReadFifo, sizeResp;
	char pidCli[MAX_INT_LENGTH];
	char * wFifo, * rFifo, * sizeStr, * message;


	signal(SIGPIPE, connectionClosed);

	
	if ((connFifo = open(connectionFifo, O_WRONLY)) == -1){
		printError(OPEN);
	}
	
	pidClient = getpid();
	
	sprintf(pidCli, "%d", pidClient);

	sizePid = strlen(pidCli);

	connFifo = open(connectionFifo, O_WRONLY);
	
	if( write(connFifo, pidCli, sizePid + 1) <0 ){
		printError(WRITE);
	}

	close(connFifo);


	//The server wakes up, reads the pid and makes a fork


	//Creates the wfifo -where the client will send the request-
	//wfifo = clientWFifo + pidClient


	wFifo = createPath(clientWFifo, pidCli);
	
	createFifo(wFifo);
	
	fdWriteFifo = open(wFifo, O_WRONLY);
	
	if( fdWriteFifo < 0 ){ 
		printError(OPEN);
	}


	//Sends the message

	if( write(fdWriteFifo, msg, size + strlen(msg) + 1) < 0 )
		printError(WRITE);
	

	//Creates the rfifo -where the client will read the
	//answer of the request-
	//rfifo = clientRFifo + pidClient

	rFifo = createPath(clientRFifo, pidCli);

	createFifo(rFifo);

	fdReadFifo = open(rFifo, O_RDONLY);


	if( fdReadFifo < 0 )
		printError(OPEN);


	//Reads the answer. Locks until the server sends it
	sizeStr = calloc(MAX_INT_LENGTH, sizeof(char));

	getStr(fdReadFifo, sizeStr, '\0');

	sscanf(sizeStr, "%d", &sizeResp);
	
	message = calloc(sizeResp, sizeof(char));

	if( read(fdReadFifo, message, sizeResp) < 0 )
		printError(READ);

	memcpy(ans, sizeStr, strlen(sizeStr) + 1);
	
	memcpy(ans + strlen(sizeStr) + 1, message, sizeResp);

	close(fdReadFifo);
	
	remove(rFifo);
	
	remove(wFifo);
	
	free(sizeStr);

	free(message);

	return;
}


char * receive(connection* con){
	char * pidCli, * readFifo, * ans, * sizeStr, * message;
	int fdReadFifo, sizeResp;


	//Reads the request
	pidCli = calloc(MAX_INT_LENGTH, sizeof(char));
	
	sprintf(pidCli,"%d", con->id);

	readFifo = createPath(clientWFifo, pidCli);
	
	fdReadFifo = open(readFifo, O_RDONLY);

	if( fdReadFifo < 0 ){
		printError(OPEN);
	}

	sizeStr = calloc(MAX_INT_LENGTH, sizeof(char));

	getStr(fdReadFifo, sizeStr, '\0');

	sizeResp = atoi(sizeStr);
	
	message = calloc(sizeResp, sizeof(char));
	
	if( read(fdReadFifo, message, sizeResp) < 0 )
		printError(READ);


	//Saves the request and returns it
	ans = calloc(strlen(sizeStr) + 1 + sizeResp, sizeof(char));
	
	memcpy(ans, sizeStr, strlen(sizeStr) + 1);
	
	memcpy(ans + strlen(sizeStr) + 1, message, sizeResp);

	close(fdReadFifo);

	free(sizeStr);

	free(message);

	return ans;

}


void respond(connection * con, char ans[MAX_SIZE], int ansSize){
	char * pidCli, * writeFifo;
	int fdWriteFifo;


	pidCli = calloc(MAX_INT_LENGTH, sizeof(char));
	
	sprintf(pidCli, "%d", con->id);

	writeFifo = createPath(clientRFifo, pidCli);
	
	signal(SIGPIPE, connectionClosed);

	fdWriteFifo = open(writeFifo, O_WRONLY);

	if( fdWriteFifo < 0 ){
		printError(OPEN);
	}
	
	if( write(fdWriteFifo, ans, ansSize + strlen(ans) + 1) < 0)
		printError(WRITE);

	close(fdWriteFifo);
}


void endServer(void){

	if( getpid() == pidFather ){
		remove(connectionFifo);
	}	
}


int getStr(int fd, char * buf, char limit) {
	char c;
	int i=0, aux;
	

	do{
		aux = read(fd, buf+i, 1);
		c = *(buf+i);
		i++;
	} while((c != limit) && (aux!=0));

	if(aux>0){
		buf[i-1]='\0';
		return i;
	} else{
		return 0;
	}
}


void connectionClosed(){
	printf("Cannot connect. The connection is closed.");
}


void createFifo(char * name){
	if ( access(name, 0) == -1 && mknod(name, S_IFIFO|0777, 0) == -1 )
			printError(MKNOD);
}


char * createPath(char * s1, char * s2){
	char * path = calloc(strlen(s1)+1, sizeof(char));
	
	strcpy(path, s1);
	
	strcat(path, s2);

	return path;
}


void printError(int errorCode){
	switch(errorCode){
		case OPEN: printf("Error openning fifo\n");
					break;
		case READ: printf("Error reading fifo\n");
					break;
		case WRITE: printf("Error writing fifo\n");
					break;
		case MKNOD: printf("Error mknod\n");
	}
}