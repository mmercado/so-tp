//fifos.h

#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include "../communication.h"

#define MAX_INT_LENGTH 10

typedef enum {OPEN=3, READ, WRITE, MKNOD} errorCode;

static int serverStarted = FALSE;
static char * connectionFifo = "./connFifo";
static char * clientWFifo = "./cliwfifo";
static char * clientRFifo = "./clirfifo";



/*
 * Reads from the origin fd until the limit char
*/
int getStr(int origin, char * buf, char limit);

/*
 * Prints a message when a closed fifo is tried to be written
*/
void connectionClosed();

/*
 * Creates a fifo with the name received
*/
void createFifo(char * name);

/*
 * Creates with the client's id the path for the fifos where the client
 * will write the request and receive the answer
*/
char * createPath(char * s1, char * s2);

/*
 * Prints a suitable error message depending on the errorCode received
*/
void printError(int errorCode);