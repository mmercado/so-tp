#include "communication.h"

/*
 * Leaves in msg, the opCode contained in the datagram (as a string), and
 * copies all the arguments at the end.
 * Writes the size of the opCode + arguments, at the start of msg.
*/

int marshalling(char msg[MAX_SIZE], datagram * data, void * args);

/*
 * Leaves in ansArgs, the answer struct.
*/
int unmarshalling(char ans[MAX_SIZE], datagram * data, void * ansArgs); 


/*
 * Makes a request to the server.
*/
void * request(int opCode, void * args);



void * request(int opCode, void * args){

	/*Used to communicate between client and server*/
	char msg[MAX_SIZE], ans[MAX_SIZE];

	/*Used to return the answer to the client*/
	void * ansArgs = calloc(MAX_SIZE, sizeof(char));
	
	int msgSize;

	datagram data;

	data.opCode = opCode;

	msgSize = marshalling(msg, &data, args);
		
	sendMsg(msg, msgSize, ans);

	if( unmarshalling(ans, &data, ansArgs) == TRUE){
		return ansArgs;
	}

	return NULL;
}


int marshalling(char msg[MAX_SIZE], datagram * data, void * args){

	char aux[MAX_SIZE];
	int msgSize = 0, index = 0;

	msgSize += sprintf(aux, "%d", data->opCode) + 1;


	switch (data->opCode){
	 		
		case BUY:

			msgSize += sizeof(argsBuy); 

			index += sprintf(msg, "%d", msgSize) + 1;
			
			index += sprintf(msg + index, "%d", data->opCode) + 1;
			
			memcpy(msg + index, (argsBuy *) args, sizeof(argsBuy));
			
			break;
		
		case SHOWMOVIES:

			index += sprintf(msg, "%d", msgSize) + 1;
			
			index += sprintf(msg + index, "%d", data->opCode) + 1;

			strcpy(msg + index, "");
			
			break;
		
		case AVAILSITS:

			msgSize += sizeof(argsAvailSits); // Le agrego el 0?? + 1;

			index += sprintf(msg, "%d", msgSize) + 1;
			
			index += sprintf(msg + index, "%d", data->opCode) + 1;
			
			memcpy(msg + index, (argsAvailSits *) args, sizeof(argsAvailSits));
		
			break;

		case VIEWDESCR:

			msgSize += sizeof(argsViewDescr); // Le agrego el 0?? + 1;

			index += sprintf(msg, "%d", msgSize) + 1;
			
			index += sprintf(msg + index, "%d", data->opCode) + 1;
			
			memcpy(msg + index, (argsViewDescr *) args, sizeof(argsViewDescr));

			break;	
		
		case RETURN:

			msgSize += sizeof(argsRetTickets);

			index += sprintf(msg, "%d", msgSize) + 1;
			
			index += sprintf(msg + index, "%d", data->opCode) + 1;
			
			memcpy(msg + index, (argsRetTickets *) args, sizeof(argsRetTickets));
		
			break;
		
		default:
		
			break;
	}

	return msgSize;

}


int unmarshalling(char ans[MAX_SIZE], datagram * data, void * ansArgs){

	int ansSize;

	ansSize = atoi(ans);
	
	if(ansSize > 0){

		memcpy(ansArgs, ans + strlen(ans) + 1, ansSize);
		return TRUE;
	}
	
	return FALSE;
}