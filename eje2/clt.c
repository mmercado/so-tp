#include "API.h"
#include <sys/types.h>
#include <unistd.h>


void * request(int opCode, void * args);

void * buy(char * movie, int amount, int hour){
	
	int i;
	argsBuy args;
	ansBuy * ans;

	args.amount = amount;
	args.hour = hour;
	strcpy(args.movie, movie);

	ans = (ansBuy*) request(BUY, &args);

	if(ans != NULL){
		
		for(i=0; i<amount; i++){
			printf("ticketID: %d, PID: %d\n", ans->ticketID[i], getpid());
		}
	}
	else{
		printf("Invalid query\n");
	}
	
	free(ans);
	return NULL;

}

void * availableSits(char * movie, int hour){

	argsAvailSits args;
	ansAvailSits * ans;

	args.hour = hour;
	strcpy(args.movie, movie);

	ans = (ansAvailSits*) request(AVAILSITS, &args);

	if(ans != NULL){

		printf("Amount: %d\n", ans->amount);		
	}
	else{

		printf("Invalid query\n");
	
	}
	free(ans);
	return NULL;
}

void * viewDescription(char * movie){

	argsViewDescr args;
	ansViewDescr * ans;

	strcpy(args.movie, movie);

	ans = (ansViewDescr *) request(VIEWDESCR, &args);

	if(ans != NULL){

		printf("Description: %s\n", ans->descr);		
	}
	else{

		printf("Invalid query\n");
	
	}
	free(ans);
	return NULL;
}

void * showMovies(void){

    int i;

    ansSM * ans;

    ans = (ansSM*) request(SHOWMOVIES, NULL);

    printf("Listing:\n");

	for(i=0; i<ans->amount; i++){
    	printf("Movie: %s, Hour: %d\n", ((ans->listFunctions)[i]).movie , ((ans->listFunctions)[i]).hour);
    }

    free(ans);

    return NULL;

}

void * returnTickets(int idTickets[], int amount){

	int i;
	argsRetTickets args;
	ansRetTickets * ans;

	args.amount = amount;
	
	for( i=0; i < amount; i++){
		args.tickets[i] = idTickets[i];
	}

	ans = (ansRetTickets*) request(RETURN, &args);

	if( ans->success == FALSE ){
		printf("Invalid query\n");
	} else {
		printf("Operation Succesful.\n");
	}

	return NULL;
}