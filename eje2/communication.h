#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

/*
 * Includes
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include <sys/types.h>
#include <unistd.h>


#define MAX_SIZE 1000
#define TRUE 1
#define FALSE 0

/*
 * Structs
*/

/* Holds the number of operation corresponding to the client's request */
typedef struct{	
	int opCode;
} datagram;

/* Holds necesary data to start a connection between client and server */
typedef struct{
	int id;
} connection;


/*
 * Functions
*/


/*
 * Sends the operation serialized in a string.
*/
void sendMsg(char * args, int msgSize, char ans[MAX_SIZE]);

/*
 * Starts a connection with a client and saves the client's id
 * into the given struct
*/
void acceptCon(connection * con);

/*
 * Receives the client's request and returns it in a string
*/
char * receive(connection * con);

/*
 * Receives the answer to the client's request in ans and its size and
 * sends it to the client.
*/
void respond(connection * con, char ans[MAX_SIZE], int ansSize);

/*
 *  Frees the resources when the server ends.
*/
void endServer();


#endif /* COMMUNICATION_H_ */