//file.c

#include "file.h"


static int sendfd, pidsfd;
static int started = FALSE;
static sigset_t intmask;
static sigset_t oldmask;

static long pidCli;
static struct sigaction act;

void emptyHand2(int sig){
    signal(SIGUSR2, emptyHand2);
}

void startConnectionCli(void){
    //creat file to client with ypur consult 
    char * init;
    init = calloc(SIZE_FOR_FILE,sizeof(char));
    sprintf(init,"send-%d", (int)(getpid()));
    sendfd = open(init,O_RDWR|O_CREAT|O_TRUNC,0777);
    free(init);
}

void startConnectionServ(void){
    //craet a file with sever's PID 
    int fd;
    char* pid;
    pid = calloc(SIZE_FOR_FILE,sizeof(char));
    sprintf(pid,"%d",getpid());
    fd = open("servPID",O_RDWR|O_CREAT|O_TRUNC,0777);
    write(fd, pid,strlen(pid) + 1);
    free(pid);
}
  

//handler to trap signal 
void hdl (int sig, siginfo_t *siginfo, void *context)
{
    pidCli=(long)siginfo->si_pid;
    act.sa_sigaction = hdl;
    act.sa_flags = SA_SIGINFO;
    sigaction(SIGUSR1, &act, NULL);
}


void sendMsg(char* msg, int size, char ans[MAX_SIZE]){
    //send msg to server
    signal(SIGUSR2, emptyHand2);

    if(!started){

        started = TRUE;
        sigemptyset(&intmask);
        sigaddset(&intmask, SIGUSR2);
        sigprocmask(SIG_BLOCK, &intmask,&oldmask);
    
    }
    

    char * rcvmsg, * sizeStr, * servPID; 
    int sizeResp, rcvfd, fdPidServ, pidServ; 

    //creat file

    startConnectionCli();
    

    servPID = calloc(SIZE_FOR_FILE,sizeof(char));
    sizeStr = calloc(10,sizeof(char));
    rcvmsg=calloc(SIZE_FOR_FILE,sizeof(char));

    //write
    write(sendfd,msg,size+strlen(msg)+1);


    //open file with server's PID

    fdPidServ=open("servPID", O_RDONLY,0777);
    if (fdPidServ==-1){ 
         perror("SEND ERROR: ");
         exit(1);
    }


    lseek(fdPidServ, 0, SEEK_SET);
    getStr(fdPidServ,servPID,'\0');
    sscanf(servPID,"%d",&pidServ);


    close(sendfd);
    //send signal to wake up server
    kill(pidServ, SIGUSR1);

    //creat mask to trap the signal

    sigsuspend(&oldmask);
   
    //i m wake up, then i going to read the answer

    sprintf(rcvmsg,"rcv-%d", getpid());
    
    if( (rcvfd=open(rcvmsg,O_RDONLY,0777)) == -1){
        perror("open: ");
        exit(1);
    }

    lseek(rcvfd,0, SEEK_SET);

    getStr(rcvfd,sizeStr,'\0');

    sscanf(sizeStr,"%d",&sizeResp);
    
    lseek(rcvfd,0, SEEK_SET);

    read(rcvfd, ans, sizeResp+strlen(sizeStr)+1);

    //delete files and free memeory
    close(sendfd);
    close(rcvfd);
    unlink(rcvmsg);



    free(sizeStr);
    free(servPID);
    free(rcvmsg);

}

void endServer(void){
    system("rm servPID");
    system("rm send-*");
    system("rm rcv-*");
    system("pids");
    close(pidsfd);
}

//this function read from file descriptor to find a limit

 int getStr(int fdOrigin, char* buf, char limit){
    char c;
    int i=0;
    int countRead;
    do{
      countRead = read(fdOrigin, buf+i, 1);
      c = *(buf+i);
      i++;
    } while((c!=limit) && (countRead>0));

    if (c==limit){

        buf[i-1]='\0';
        return i-1;
    }else {
        return 0;
    }

}

static DIR * d;
static int isOpened = FALSE;
static struct dirent * dir;

void acceptCon(connection * con){
    
    int pid;

    if (!started){
        
        //prepare the mask when is the first cicle of server 
        
        memset (&act, '\0', sizeof(act));
        act.sa_sigaction = hdl;
        act.sa_flags = SA_SIGINFO;
        sigaction(SIGUSR1, &act, NULL);

        startConnectionServ();
        started=TRUE;
        sigemptyset(&intmask);
        sigaddset(&intmask, SIGUSR1);
        sigprocmask(SIG_BLOCK, &intmask,&oldmask);
        
        //when client wake up me, i save all files with consults in pid
        sigsuspend(&oldmask);
        
    }
         
    if(!isOpened){
        isOpened = TRUE;
        if( (d = opendir (".")) == NULL){
            perror("Error opening directory: ");
            printf("Exiting...\n");
            exit(1);
        }
    
    }

    while ((dir = readdir (d)) != NULL) {

        if(sscanf(dir->d_name, "send-%d", &pid) > 0){
            con->id = pid;
            return;

        }
    }

    closedir (d);
    isOpened = FALSE;
    sigsuspend(&oldmask);

     
}



char* receive(connection* con){
    char *resp;
    int size, consult;
    char* sizeStr; 
    char* fileStr;
    
    if(isOpened){
        closedir (d);
    }else{
        exit(1);
    }
    
    fileStr = calloc(SIZE_FOR_FILE, sizeof(char));
    sizeStr=calloc(MAGICNUMBER, sizeof(char));   
    
    sprintf(fileStr,"send-%d",con->id);
   
    //open file send-pid
    consult = open(fileStr, O_RDONLY,0777);
    if (consult==-1){ 
         perror("RECEIVE ERROR: ");
         exit(1);
    }
   
    lseek(consult, 0, SEEK_SET);
    // read size of consult
    getStr(consult,sizeStr,'\0');

    sscanf(sizeStr,"%d",&size);

    resp = calloc(size+strlen(sizeStr)+1,sizeof(char));

    lseek(consult, 0, SEEK_SET);

    read(consult, resp, size+strlen(sizeStr)+1);

    close(consult);

    unlink(fileStr);

    free(fileStr);

    free(sizeStr);

    return resp;
 }

 void respond(connection * con, char ans[MAX_SIZE], int ansSize) {
    char* aux;

    aux = calloc(SIZE_FOR_FILE,sizeof(char));

    sprintf(aux,"rcv-%d",con->id);

    int respfd;
   
    if( (respfd = open(aux, O_CREAT|O_WRONLY|O_TRUNC,0777)) == -1){
        perror("respond, open: ");
        exit(1);
    }
    

    write(respfd, ans, ansSize+strlen(ans)+1);

    //wake up client

    kill(con->id,SIGUSR2);

    close(respfd);

    free(aux);
    

 }