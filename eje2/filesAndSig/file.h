//file.h
#include "../communication.h"

#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#define SIZE_FOR_FILE 30
#define MAGICNUMBER 6

void startConnectionCli(void);
void startConnectionServ(void);
void emptyHand1(int sig);
void emptyHand2(int sig);
int getStr(int origin, char* buf, char limit);
int findPID(int fd);