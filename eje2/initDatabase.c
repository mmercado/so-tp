//initDatabase
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "../sqlite/sqlite3.h"

void initMovies(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data);

void initFunctions(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data);

void initSold(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data);

void initTickets(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data);

static int callback(void *NotUsed, int argc, char **argv, char **azColName);


static int callback(void *NotUsed, int argc, char **argv, char **azColName){
   int i;
   for(i=0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

int
main(void){

	sqlite3 * db;
	int rc;
	char * sql = NULL;
	char * data = "Callback function called";
	char * zErrMsg = 0;

  /*Delete the db if it already existed*/
  unlink("cinema.db");

	rc = sqlite3_open("cinema.db", &db);



    if( rc ){
    	fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
    	exit(0);
    }
    else{
    	fprintf(stderr, "Opened database successfully\n");
    }

    initMovies(db, sql, rc, zErrMsg, data);
    initFunctions(db, sql, rc, zErrMsg, data);
    initSold(db, sql, rc, zErrMsg, data);
    initTickets(db, sql, rc, zErrMsg, data);

	sqlite3_close(db);

  return 0;

}

void initMovies(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data){
	
	sql = "CREATE TABLE MOVIE("  \
         "NAME TEXT PRIMARY KEY     NOT NULL," \
         "DESCR           TEXT    NOT NULL);";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
   fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   }
   else{
      fprintf(stdout, "Table created successfully\n");
   }

	sql = "INSERT INTO MOVIE (NAME,DESCR) "  \
         "VALUES ('HarryPotter', 'Hogwarts School of Witchcraft and Wizardry'); " \
         "INSERT INTO MOVIE (NAME,DESCR) "  \
         "VALUES ('StarWars', 'Use the force'); " \
         "INSERT INTO MOVIE (NAME,DESCR) "  \
         "VALUES ('ET', 'UFO');";

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else{
		fprintf(stdout, "Records created successfully\n");
	}
	
}

void initFunctions(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data){

	sql = "CREATE TABLE FUNCTIONS("  \
         "NAME TEXT NOT NULL," \
         "TIME INT NOT NULL," \
         "AVAILSITS INT NOT NULL," \
         "PRIMARY KEY(NAME, TIME) );";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
   fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   }
   else{
      fprintf(stdout, "Table created successfully\n");
   }

	sql = "INSERT INTO FUNCTIONS (NAME, TIME, AVAILSITS) "  \
         "VALUES ('ET', 12, 100); " \
         "INSERT INTO FUNCTIONS (NAME, TIME, AVAILSITS) "  \
         "VALUES ('HarryPotter', 20, 100); " \
         "INSERT INTO FUNCTIONS (NAME, TIME, AVAILSITS) "  \
         "VALUES ('StarWars', 14, 100); " \
         "INSERT INTO FUNCTIONS (NAME, TIME, AVAILSITS) "  \
         "VALUES ('StarWars', 21, 100); " ;

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else{
		fprintf(stdout, "Records created successfully\n");
	}
}


void initSold(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data){

	sql = "CREATE TABLE SOLD("  \
         "TICKETID INT PRIMARY KEY NOT NULL," \
         "NAME TEXT NOT NULL," \
         "TIME INT NOT NULL);";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
   fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   }
   else{
      fprintf(stdout, "Table created successfully\n");
   }

}

void initTickets(sqlite3 *db, char * sql, int rc, char * zErrMsg, char * data){

	sql = "CREATE TABLE TICKETS("  \
         "TICKET INT PRIMARY KEY NOT NULL);";

   /* Execute SQL statement */
   rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
   fprintf(stderr, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
   }
   else{
      fprintf(stdout, "Table created successfully\n");
   }

	sql = "INSERT INTO TICKETS (TICKET) "  \
         "VALUES (1); ";

	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else{
		fprintf(stdout, "Records created successfully\n");
	}
}
