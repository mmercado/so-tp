#include "communication.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <signal.h>

/*
 * Retrieves the request from the client, makes the query to the database and   
 * returns the answer.
*/
int unmarshalling(char * msg, char ans[MAX_SIZE]);

/*
 * Frees the resources in case the server is killed.
*/
void intHandler(int sig);

void lockDB(void);

void unLockDB(void);

/* Used for communication with sockets */
char serverIP[15];

/* Used for communication with files */
static int fd;


int main(){

	signal(SIGINT, intHandler);
	
	while (TRUE){
		
		int pid, ansSize;
		connection newCon;
		char * msg;
		char ans[MAX_SIZE];
		
		acceptCon(&newCon);
		
		switch ( pid = fork() ){
			
			case -1:
				
				printf("Fork Error");
				exit(1);
				break;

			case 0: /* Child */

				msg = receive(&newCon);
				
				ansSize = unmarshalling(msg, ans);

				respond(&newCon, ans, ansSize);

				return 0;
				
				break;
				
			default: /* Parent */
				
				break;
		}
	}

	return 0;	
}



int unmarshalling(char * msg, char ans[MAX_SIZE]){

	int ansSize, opCode, index = 0;

	void * msgArgs = malloc(100);
	void * ansArgs = malloc(100);
	
	index += strlen(msg) + 1; /* Skip the size */

	opCode = atoi(msg + index);

	index += strlen(msg + index) + 1; /* Skip the opcode*/

	switch (opCode){
 		
 		case BUY:
 			
 			memcpy(msgArgs, msg + index, sizeof(argsBuy));
 			
 			lockDB();

 			ansArgs = (ansBuy *) buy( ((argsBuy *)msgArgs)->movie , ((argsBuy *) msgArgs)->amount, ((argsBuy *) msgArgs)->hour);
	
 			unLockDB();

 			if(ansArgs != NULL){

	 			ansSize = sizeof(ansBuy);
	 			
	 			sprintf(ans, "%d", ansSize);
				
				memcpy(ans + strlen(ans) + 1, ansArgs, ansSize);					
			}
			else{
		
				ansSize = 0;
				sprintf(ans, "%d", ansSize);
			}	

 			break;
 		
 		case SHOWMOVIES:

 			lockDB();

 			ansArgs = (ansSM *) showMovies();

 			unLockDB();
 			
 			if(ansArgs != NULL){

	 			ansSize = sizeof(ansSM);
	 			
	 			sprintf(ans, "%d", ansSize);
				
				memcpy(ans + strlen(ans) + 1, ansArgs, ansSize);					
			}
			else{
				
				ansSize = 0;
				sprintf(ans, "%d", ansSize);
			}

 			break;
 		
 		case AVAILSITS:

 			memcpy(msgArgs, msg + index, sizeof(argsAvailSits));

 			lockDB();

 			ansArgs = (ansAvailSits *) availableSits( ((argsAvailSits *)msgArgs)->movie , ((argsAvailSits *) msgArgs)->hour);
 			
 			unLockDB();

 			if(ansArgs != NULL){

	 			ansSize = sizeof(ansAvailSits);
	 			
	 			sprintf(ans, "%d", ansSize);
				
				memcpy(ans + strlen(ans) + 1, ansArgs, ansSize);					
			}
			else{
				ansSize = 0;
				sprintf(ans, "%d", ansSize);
			}
 			break;

 		case VIEWDESCR:

 			memcpy(msgArgs, msg + index, sizeof(argsViewDescr));

 			lockDB();

 			ansArgs = (ansViewDescr *) viewDescription( ( (argsViewDescr *)msgArgs)->movie );
 			
 			unLockDB();

 			if(ansArgs != NULL){

	 			ansSize = sizeof(ansViewDescr);
	 			
	 			sprintf(ans, "%d", ansSize);
				
				memcpy(ans + strlen(ans) + 1, ansArgs, ansSize);					
			}
			else{
				ansSize = 0;
				sprintf(ans, "%d", ansSize);
			}


 			break;	
 		
 		case RETURN:

 			memcpy(msgArgs, msg + index, sizeof(argsRetTickets));

 			lockDB();

 			ansArgs = (ansRetTickets *) returnTickets( ((argsRetTickets *)msgArgs)->tickets , ((argsRetTickets *) msgArgs)->amount);
 			
 			unLockDB();

 			if(ansArgs != NULL){

	 			ansSize = sizeof(ansRetTickets);
	 			sprintf(ans, "%d", ansSize);
				
				memcpy(ans + strlen(ans) + 1, ansArgs, ansSize);
			}
			else{
				
				ansSize = 0;
				sprintf(ans, "%d", ansSize);
			}

 			break;
 		
 		default:
 			break;		
 	}

 	return ansSize;
}

void intHandler(int sig) {

	endServer();
    exit(1);

}

void lockDB(void){
	struct flock lock;
	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	fd = open("AccessDataBase.txt",O_CREAT|O_RDWR, 0666);
	if ( fd == -1 )
		perror("open");	
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("MI ERROR lock: ");

}

void unLockDB(void){
	struct flock lock;
	lock.l_type = F_UNLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	if ( fcntl(fd,F_SETLKW, &lock) == -1 )
		perror("MI ERROR lock: ");
	close(fd);
}
