//Sockets.h

#include <stdio.h>          
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>   
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <unistd.h>
#include "../communication.h"

#define PORT 3550 /* Number of port that will be openned */
#define BACKLOG 3 /* Max number of connections allowed */
#define MAX_INT_LENGTH 10


/*
 * Creates the socket where the server will receive the clients' requests 
*/
void startConnectionServ();

/*
 * Connects to the server's socket
*/
void startConnectionCli(const char* ip);

void getSize(int origin, char * buf);

/*
 * Returns the IP of the current device
*/
int knowIP(void);