#include "sockets.h"

static int fd;
static struct sockaddr_in* server;
static int serverStarted = FALSE;
char serverIP[15];



void acceptCon(connection* con){
    int sin_size;
    struct sockaddr_in client;
    sin_size =sizeof(struct sockaddr_in);


    if (!serverStarted){
      knowIP();
      startConnectionServ();
      serverStarted = TRUE;
    }

    if ((con->id = accept(fd,(struct sockaddr *)&client,(socklen_t*)&sin_size))==-1) {
       printf("Error accept\n");
       exit(-1);
    }
 }


char * receive(connection* con){
  char* params, * resp, * sizeStr;
  int size;


  sizeStr = calloc(MAX_INT_LENGTH,1);
  
  getSize(con->id, sizeStr);
  
  size = atoi(sizeStr);
  
  params = malloc(size);
  
  if( read(con->id, params, size) < 0 )
    printf("Error reading\n");

  resp = malloc(size+strlen(sizeStr)+1);
  
  memcpy(resp,sizeStr, strlen(sizeStr)+1);
  
  memcpy(resp+strlen(sizeStr)+1, params, size);
  
  return resp;
}


void respond(connection* con, char msg[MAX_SIZE], int size){
    if( write( con->id, msg, size+strlen(msg)+1) < 0 )
      printf("Error writing\n");
    close( con->id );
 }


void sendMsg(char msg[MAX_SIZE], int size, char ans[MAX_SIZE]){
  char * respond;
  int written;
  connection con;
  
  startConnectionCli(serverIP);

  written = write(fd, msg, size + strlen(msg) + 1);

  if( written < 0 ){
    printf("Error writing\n");
  }

  con.id = fd;
  
  respond = receive(&con);
  
  memcpy(ans, respond, strlen(respond)+1+atoi(respond));
  
  close(fd);
  
  free(server);
}


void endServer(){
  close(fd);
}


void getSize(int origin, char * buf){
    char c;
    int i=0;
    do{
      read( origin, buf+i, 1);
      c = *(buf+i);
      i++;
    } while(c!='\0');
}


int knowIP(void){
  struct ifaddrs * ifAddrStruct = NULL;
  struct ifaddrs * ifa = NULL;
  void * tmpAddrPtr = NULL;


  getifaddrs(&ifAddrStruct);

  for ( ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next ){
      if ( !ifa->ifa_addr ) {
          continue;
      }
      /* Checks it is IP4 */
      if ( ifa->ifa_addr->sa_family == AF_INET ){
          char addressBuffer[INET_ADDRSTRLEN];
          
          tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
          
          inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
          printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
          
      }
      /* Checks it is IP4 */
        else if (ifa->ifa_addr->sa_family == AF_INET6){
          char addressBuffer[INET6_ADDRSTRLEN];
          
          tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
          
          inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
          printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
         
      } 
  }

  if (ifAddrStruct!=NULL)
    freeifaddrs(ifAddrStruct);
  
  return 0;
}


void startConnectionCli(const char* ip){
  struct hostent *he; /* Structure used to receive information about the remote node */         
  

  if ( (he = gethostbyname(ip)) == NULL ){       
    printf("gethostbyname() error\n");
    exit(-1);
  }

  if ( (fd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){  
     printf("socket() error\n");
     exit(-1);
  }

  server = malloc(sizeof(struct sockaddr_in));
  
  server->sin_family = AF_INET;
  
  server->sin_port = htons(PORT); 
  
  server->sin_addr = *((struct in_addr *)he->h_addr);  
  
  bzero(&(server->sin_zero), 8);

  if( connect( fd, (struct sockaddr *)server,sizeof(struct sockaddr)) == -1 ){ 
     printf("connect() error\n");
     exit(-1);
  }
}


void startConnectionServ(){
  if ( (fd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ) {  
     printf("socket() error\n");
     exit(-1);
  }
  
  server = malloc(sizeof(struct sockaddr_in));
  
  server->sin_family = AF_INET;         
  
  server->sin_port = htons(PORT); 
  
  server->sin_addr.s_addr = INADDR_ANY;
  
  bzero( &(server->sin_zero), 8 ); 
  
  if( bind(fd,(struct sockaddr*)server,sizeof(struct sockaddr)) == -1 ) {
     printf("bind() error \n");
     exit(-1);
  }     
  
  if( listen(fd,BACKLOG) == -1 ) {
     printf("listen() error\n");
     exit(-1);
  }
}