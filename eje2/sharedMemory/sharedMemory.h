#ifndef SHARED_MEMORY_H_
#define SHARED_MEMORY_H_

/*
 * Includes
*/

#include "../communication.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

/*
 * Constants
*/

static const key_t semkey =  0xFA8BCEBA;
static const key_t memkey =  0xFA8DCEBB;

/*
 * Structs
*/


union semun {
int val;
struct semid_ds *buf;
unsigned short int *array;
struct seminfo *__buf;
};


/*
 * Functions
*/


/*
 * Receives a key and and an integer which is used to store the memid. 
 * Returns a pointer to the shared memory area.
*/
char * openConnection(int key, int * memid, int flags);


/*
 * Returns the semid (Creates the semaphore if it didn't exist)
*/
int getSem(int key, int amount, int flags);

/*
 * Set initial values for the semaphores
*/
void iniCommonSem();

void iniClientSem(int semid);

/*
 * Decreases de semaphore count
*/

void down(int semNum, int mySemid);


/*
 * Increases de semaphore count
*/

void up(int semNum, int mySemid);

#endif /* SHARED_MEMORY_H_ */
