#include "sharedMemory.h"

#define SIZE 2000

static int clientStarted = FALSE;
static int serverStarted = FALSE;

static int commonSemid;
static int commonMemid;
static char * commonArea; /* Pointer to the communication zone shared by Server and Clients */

void sendMsg(char msg[MAX_SIZE], int msgSize, char ans[MAX_SIZE]){
 	
	int cliSemid, cliMemid;
	char * cliMem;

	connection con;
 	con.id = getpid();
 	
 	if(!clientStarted){
 		
 		commonArea = openConnection(memkey, &commonMemid, 0666);
		commonSemid = getSem(semkey, 3, 0666);
 		clientStarted = TRUE;
 	}


 	/*Create the client's memory area and semaphores*/
 	cliMem = openConnection(con.id, &cliMemid, IPC_CREAT|IPC_EXCL|0666);
 	
 	memset(cliMem, 0, SIZE);
 	
 	cliSemid = getSem(con.id, 2, IPC_CREAT|IPC_EXCL|0666);
 	
 	iniClientSem(cliSemid);

 	down(0, commonSemid); /*commonArea is blocked*/	

 	memcpy(commonArea, &con, sizeof(int)); /* Copy the connection data to the server */

	up(1, commonSemid); /* Wake server to accept the connection */
 	
 	down(2, commonSemid); /* Client sleeps */

 	up(0, commonSemid); /* Unlock the common zone */	 		

 	memcpy(cliMem, msg, msgSize + strlen(msg) + 1);

 	up(1, cliSemid); /* Wake server to process the query */

 	down(0, cliSemid); /* Client sleeps */

 	/* Copy the answer of the operation */

 	memcpy(ans, cliMem, atoi(cliMem) + strlen(cliMem) + 1);

 	shmdt(cliMem);

 	shmctl (cliMemid, IPC_RMID, 0);
	
	union semun ignoredArg;

 	semctl (cliSemid, 0, IPC_RMID, ignoredArg); /* Detach the semaphores */
 	semctl (cliSemid, 1, IPC_RMID, ignoredArg);

	return;
}


char * openConnection(int key, int * memid, int flags){
	
	char * mem;

	if ( (*memid = shmget(key, SIZE, flags)) == -1 ){
		printf("error shmget");
		perror("Error: ");
		exit(1);
	}	

	if ( (mem = shmat(*memid, NULL, 0)) < 0){
		printf("error shmat");
	}
	
	return mem;
}


int getSem(int key, int amount, int flags){

	int semid;

	if ( (semid = semget(key, amount, flags)) == -1 ){
		printf("error semget");
	}

	return semid;	
	
}

void iniCommonSem(){
	
	semctl(commonSemid, 0, SETVAL, 1);
	semctl(commonSemid, 1, SETVAL, 0);
	semctl(commonSemid, 2, SETVAL, 0);

}

void iniClientSem(int semid){

	semctl(semid, 0, SETVAL, 0);
	semctl(semid, 1, SETVAL, 0);		

}

void acceptCon(connection* con){

	if(!serverStarted){
		
		commonArea = openConnection(memkey, &commonMemid, IPC_CREAT|IPC_EXCL|0666);
		
		memset(commonArea, 0, SIZE);
		
		commonSemid = getSem(semkey, 3, IPC_CREAT|IPC_EXCL|0666);
		
		iniCommonSem();
		
		serverStarted = TRUE;
	}
		
 	down(1, commonSemid); //Server sleeps, waiting for a client to connect
	
	memcpy(con, commonArea,sizeof(int));

	memset(commonArea, 0, SIZE);

	return;
}


char * receive(connection * con){
	
	int cliSemid, cliMemid, msgSize;

	char * msg;
	
	char * cliMem = openConnection(con->id, &cliMemid, IPC_CREAT|0666);

	cliSemid = getSem(con->id, 2, IPC_CREAT|0666);

	up(2, commonSemid); /* Wake Client */
	
	down(1, cliSemid); /* Server child sleeps waiting for arguments */

	msgSize = atoi(cliMem) + strlen(cliMem) + 1;

	msg = malloc(1000);

	memcpy(msg, cliMem, msgSize);
	
	return msg;
}


void respond(connection * con, char ans[MAX_SIZE], int ansSize){
	
	int cliMemid, cliSemid;
	
	char * cliMem = openConnection(con->id, &cliMemid, IPC_CREAT|0666);

	cliSemid = getSem(con->id, 2, IPC_CREAT|0666);
	
	memcpy(cliMem, ans, ansSize + strlen(ans) + 1);
	
	up(0, cliSemid);

	shmdt(cliMem);

	return;
}

void down(int semNum, int mySemid){

	struct sembuf sb;
	sb.sem_num = semNum;
	sb.sem_op = -1;
	sb.sem_flg = SEM_UNDO;
	semop(mySemid, &sb, 1);
}

void up(int semNum, int mySemid){

	struct sembuf sb;
	sb.sem_num = semNum;
	sb.sem_op = 1;
	//sb.sem_flg = SEM_UNDO;
	semop(mySemid, &sb, 1);
}

void endServer(){

	shmdt(commonArea);

 	shmctl (commonMemid, IPC_RMID, 0);
	
	union semun ignoredArg;

 	semctl (commonSemid, 3, IPC_RMID, ignoredArg);
}